========
GS PDF/A
========


.. image:: https://img.shields.io/pypi/v/gs_pdfa.svg
        :target: https://pypi.python.org/pypi/gs_pdfa

.. image:: https://img.shields.io/travis/audreyr/gs_pdfa.svg
        :target: https://travis-ci.org/audreyr/gs_pdfa

.. image:: https://readthedocs.org/projects/gs-pdfa/badge/?version=latest
        :target: https://gs-pdfa.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




PDF/A generation using Ghostscript


* Free software: GNU General Public License v3
* Documentation: https://gs-pdfa.readthedocs.io.


Installation
------------

Ghostscript is required. Please use your favorite package manager. Then:

    ``python setup.py install``


Features
--------

* Convert to PDF/A using color profiles.
* CLI


CLI usage
*********

    ``to_pdfa [OPTIONS] [PDF_FILE]``

Where options are:

* Color profile: ``--profile [rgb|cmyk]``

``PDF_FILE`` can be streamed into stdin.


Lib usage
*********


.. code-block:: python

    from gs_pdfa import PdfA
    from io import BytesIO

    pdf_a_stream = BytesIO()
    PdfA.convert_pdf(input_pdf='/tmp/my-pdf.pdf',
                     output_pdfa=pdf_a_stream)


Credits
-------

Based on Vincent Bozzo work.


This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

