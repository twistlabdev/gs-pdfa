# -*- coding: utf-8 -*-

"""Top-level package for GS PDF/A."""

__author__ = """Gregory Favre"""
__email__ = 'dev@twistlab.ch'
__version__ = '1.0.1'

from .gs_pdfa import ColorProfile, PdfA  # noqa: F401
