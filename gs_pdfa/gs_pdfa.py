# -*- coding: utf-8 -*-
"""
PDF/A conversion uses Ghostscript library.
To fully convert, the color model has to be included.

GS command: gs -dCompressFonts=true -dSubsetFonts=true -dEmbedAllFonts=true -dNOPAUSE -dBATCH' -dPDFA=1 -dNOOUTERSAVE
               -dNOSAFER -dPDFACompatibilityPolicy=1 -sDEVICE=pdfwrite -dPDFACompatibilityPolicy=1
               -sColorConversionStrategy=CMYK -sProcessColorModel=DeviceCMYK PDFA_cmyk_def.ps <input file>
"""
from enum import Enum
import os
from subprocess import check_output
import tempfile


class ColorProfile(Enum):
    """
    Class to manage the color profiles for pdf/a conversion
    """
    RGB = 0
    CMYK = 1

    def get_ps_filename(color_profile):
        ps_rgb_profile = "PDFA_rgb_def.ps"
        ps_cmyk_profile = "PDFA_cmyk_def.ps"
        if color_profile == ColorProfile.RGB:
            return ps_rgb_profile
        elif color_profile == ColorProfile.CMYK:
            return ps_cmyk_profile
        else:
            raise LookupError("Profile not found")


class PdfA:
    @staticmethod
    def convert_pdf(input_file, output_pdfa, color_profile=ColorProfile.CMYK):
        """
        Convert a pdf file to a pdf/a using postscript and Adobe profile.
        :param input_file: path to the input pdf
        :param output_pdfa: stream object implementing a write method. Beware, we'll be outputing bytes.
        :param color_profile: either ColorProfile.CMYK or ColorProfile.RGB
        :return: None
        """
        color_model = '-sProcessColorModel=Device{}'.format(str(color_profile.name))
        color_conversion = '-sColorConversionStrategy={}'.format(str(color_profile.name))

        if not os.path.isfile(input_file):
            raise FileNotFoundError("File: '{}' not found".format(input_file))

        # See https://stackoverflow.com/questions/3351967/prevent-ghostscript-from-writing-errors-to-standard-output
        args = ['gs',
                '-sstdout=%stderr',  # silence Ghostscript.
                '-dCompressFonts=true', '-dSubsetFonts=true', '-dEmbedAllFonts=true',
                '-dNOPAUSE', '-dBATCH',
                '-dPDFA=1', '-dNOOUTERSAVE', '-dNOSAFER', '-dPDFACompatibilityPolicy=1', '-sDEVICE=pdfwrite',
                '-dPDFACompatibilityPolicy=1', color_conversion, color_model,
                '-sOutputFile=-',  # redirect output to stdout
                ColorProfile.get_ps_filename(color_profile), input_file]

        pdf_stream = check_output(args, cwd=os.path.dirname(__file__))
        output_pdfa.write(pdf_stream)

    @staticmethod
    def convert_pdf_bytes(input_pdf_bytes, output_pdfa, color_profile=ColorProfile.CMYK):
        """
        Will convert an input pdf file (bytes) on the fly and return it as bytes
        This method uses tmp files
        """
        with tempfile.NamedTemporaryFile('wb', suffix=".pdf") as pdf_file:
            pdf_file.write(input_pdf_bytes.read())
            PdfA.convert_pdf(pdf_file.name, output_pdfa, color_profile)
