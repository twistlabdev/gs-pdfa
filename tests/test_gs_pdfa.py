#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `gs_pdfa` package."""
from io import BytesIO
from unittest.mock import Mock
import unittest
import tempfile

from gs_pdfa import PdfA


class PdfATests(unittest.TestCase):
    """Tests for `gs_pdfa` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self.tmp_pdf = tempfile.NamedTemporaryFile(suffix='.pdf')

    def tearDown(self):
        """Tear down test fixtures, if any."""
        self.tmp_pdf.close()

    @unittest.mock.patch('gs_pdfa.gs_pdfa.check_output')
    def test_convert_pdf_calls_gs(self, mock_subproc_check_output):
        mock_subproc_check_output.return_value = b"Help, help! I'm being repressed!"
        output = BytesIO()
        PdfA.convert_pdf(self.tmp_pdf.name, output)
        self.assertEqual(mock_subproc_check_output.call_count, 1)

    @unittest.mock.patch('gs_pdfa.gs_pdfa.check_output')
    def test_convert_pdf_writes_gs_output(self, mock_subproc_check_output):
        fake_pdf_content = b'Nobody expects the Spanish Inquisition'
        mock_subproc_check_output.return_value = fake_pdf_content
        output = BytesIO()
        PdfA.convert_pdf(self.tmp_pdf.name, output)
        self.assertEqual(output.getvalue(), fake_pdf_content)
