#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

from gs_pdfa import __version__


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

setup_requirements = []

test_requirements = []

setup(
    author="Gregory Favre",
    author_email='dev@twistlab.ch',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',

    ],
    description="PDF/A generation using Ghostscript",
    entry_points={
        'console_scripts': [
            'gs_pdfa=gs_pdfa.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='gs_pdfa',
    name='gs_pdfa',
    packages=find_packages(include=['gs_pdfa']),
    scripts=['bin/to_pdfa'],
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/twistlabdev/gs-pdfa',
    version=__version__,
    zip_safe=False,
)
